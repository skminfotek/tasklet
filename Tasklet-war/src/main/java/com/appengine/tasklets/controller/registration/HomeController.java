package com.appengine.tasklets.controller.registration;

import com.company.core.data.CoreInformation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")
public class HomeController {
    @RequestMapping(method = RequestMethod.GET)
    public String show() {
        CoreInformation inf = new CoreInformation();
        inf.callThis();
        return "homeTasklet";
    }
}