package com.appengine.tasklets.controller.home;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.ScriptAssert;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@ScriptAssert(lang = "javascript", script = "_this.conformPassword.equals(_this.password)", message = "account.password.mismatch.message")
public class AccountForm {

    private String userName, password, conformPassword, firstName, lastName, email;
    private boolean marketingOK = true;
    private boolean acceptTerms = false;

    @NotNull
    @Size(min = 6, max = 20)
    public String getUserName() {
        return userName;
    }

    @NotNull
    @Size(min = 6, max = 30)
    public String getPassword() {
        return password;
    }

    @NotNull
    @Size(min = 6, max = 30)
    public String getConformPassword() {
        return conformPassword;
    }

    @NotNull
    @Size(min = 6, max = 50)
    public String getFirstName() {
        return firstName;
    }

    @NotNull
    @Size(min = 6, max = 50)
    public String getLastName() {
        return lastName;
    }

    @NotNull
    @Size(min = 6, max = 50)
    @Email
    public String getEmail() {
        return email;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }


    @AssertTrue(message = "{account.marketingOK.assertTrue.message}")
    public boolean isMarketingOK() {
        return marketingOK;
    }


    @AssertTrue(message = "{account.acceptTerms.assertTrue.message}")
    public boolean isAcceptTerms() {
        return acceptTerms;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setConformPassword(String conformPassword) {
        this.conformPassword = conformPassword;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setMarketingOK(boolean marketingOK) {
        this.marketingOK = marketingOK;
    }

    public void setAcceptTerms(boolean acceptTerms) {
        this.acceptTerms = acceptTerms;
    }

    @Override
    public String toString() {
        return "AccountForm{" +
                "userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", conformPassword='" + conformPassword + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", marketingOK=" + marketingOK +
                ", acceptTerms=" + acceptTerms +
                '}';
    }
}
