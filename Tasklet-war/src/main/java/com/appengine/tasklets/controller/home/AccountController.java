package com.appengine.tasklets.controller.home;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * User: Sathish Jayapal
 * Date: 8/25/14
 */
@Controller
@RequestMapping("/users")
public class AccountController {
    private static final Logger log = LoggerFactory.getLogger(AccountController.class);

    @RequestMapping(value = "new", method = GET)
    public String getRegistrationForm(Model model) {
        model.addAttribute("account", new AccountForm());
        return "registrationForm";
    }

    @RequestMapping(value = "", method = POST)
    public String postRegistationForm(@ModelAttribute("account") @Valid AccountForm form, BindingResult result) {
        for (ObjectError error : result.getGlobalErrors()) {
            String msg = error.getDefaultMessage();
            if ("account.password.mismatch.message".equals(msg)) {
                if (!result.hasFieldErrors("password")) {
                    result.rejectValue("password", "error.mismatch");
                }
            }
        }
        log.info("Created Registration form", form);
        return result.hasErrors() ? "registrationForm" : "registration_ok";
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.setAllowedFields(new String[]{
                "username", "password", "conformPassword", "firstName", "lastName", "email", "marketingOK", "acceptTerms"
        });
    }
}
