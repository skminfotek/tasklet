<%--Author credits to yaw/springmvc-gae --%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content=""/>
    <meta name="copyright" content=""/>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/tasklet/css/kickstart.css" media="all"/>
    <!-- KICKSTART -->
    <link rel="stylesheet" type="text/css" href="/tasklet/css/style.css" media="all"/>
    <!-- CUSTOM STYLES -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/kickstart.js"></script>
    <script src="${pageContext.request.contextPath}/js/app.js" type="text/javascript"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=8"/>
    <title>Tasklet Samples</title>
    <title>Simple Form Validation</title>
</head>
<body>
<div class="grid flex">

    <header>
        <tiles:insertAttribute name="header" ignore="true"/>
    </header>
    <div class="col_12">
        <menu class="col_12">
            <tiles:insertAttribute name="menu" ignore="true"/>
        </menu>
        <div class="col_2"></div>
        <div class="col_8">
            <tiles:insertAttribute name="body"/>
        </div>
        <div class="col_2"></div>
        <footer>
            <tiles:insertAttribute name="footer" ignore="true"/>
        </footer>
    </div>
</div>
</body>
</html>