<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<nav class="navbar">
    <a class="hide-phone" id="logo" href="http://tasklets1.appspot.com"><i class="icon-angle-right"></i>
        Tasklet<span></span></a>
    <ul>
        <li class="Tasklet"><a href="http://tasklets1.appspot.com/"><span>Tasklet</span></a></li>
    </ul>
</nav>
<!-- ===================================== END HEADER ===================================== -->