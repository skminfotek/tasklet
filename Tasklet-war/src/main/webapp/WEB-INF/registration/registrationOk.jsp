<%--
  User: Sathish Jayapal
  Date: 8/25/14
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<h1><spring:message code="registrationOK.pageTitle"/></h1>

<p><spring:message code="registrationOK.message.thanks"/></p>