<%--
  User: Sathish Jayapal
  Date: 8/25/14
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<script type="text/javascript" src="/js/jquery.validate.min.js"></script>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<spring:message var="pageTitle" code="newUserRegistration.pageTitle"/>
<spring:message var="msgAllfieldsAreRequored" code="newUserRegistration.message.allFieldsRequired"/>

<form:form action="." modelAttribute="account">
    <form:errors path="*">
        <div class="notice error"><spring:message code="error.global"/></div>
    </form:errors>
    <p class="col_12">
            ${msgAllfieldsAreRequored}
    </p>
    <%--label for="username" class="col_2"--%>

    <div class="col_12">
        <label for="userName" class="col_2"><spring:message
                code="newUserRegistration.label.username"/></label>
        <form:input id="userName" type="input" cssClass="col_6" path="userName"
                    placeholder="userName" cssErrorClass="col_6 error"/>
        <form:errors path="userName" cssErrorClass="col_2 error">
            <%--<form:errors path="userName" htmlEscape="false" cssClass="col_4"/>--%>
        </form:errors>
    </div>

    <div class="col_12">
        <label for="password" class="col_2"><spring:message
                code="newUserRegistration.label.password"/></label>
        <form:input id="password" type="text" cssClass="col_6" cssErrorClass="col_6 error" path="password"
                    placeholder="password"/>
    </div>

    <div class="col_12">
        <label for="confirmpassword" class="col_2"><spring:message
                code="newUserRegistration.label.confirmPassword"/></label>
        <form:input id="confirmpassword" type="text" cssClass="col_6" cssErrorClass="col_6 error" path="conformPassword"
                    placeholder="confirm password"/>
    </div>

    <div class="col_12"><label for="email" class="col_2"><spring:message
            code="newUserRegistration.label.email"/></label>
        <form:input id="email" type="text" cssClass="col_6" cssErrorClass="col_6 error" path="email"
                    placeholder="Email address"/></div>

    <div class="col_12"><label for="firstName" class="col_2"><spring:message
            code="newUserRegistration.label.firstname"/></label>
        <form:input id="firstName" type="text" cssClass="col_6" cssErrorClass="col_6 error" path="firstName"
                    placeholder="First Name"/></div>

    <div class="col_12"><label for="lastName" class="col_2"><spring:message
            code="newUserRegistration.label.lastName"/></label>
        <form:input id="lastName" type="text" cssClass="col_6" cssErrorClass="col_6 error" path="lastName"
                    placeholder="Last Name"/></div>

    <div class="col_12"><label for="marketingOK" class="col_6"><spring:message
            code="newUserRegistration.label.marketingOK"/></label>
        <form:checkbox id="marketingOK" cssClass="checkbox" cssErrorClass="checkbox error" path="marketingOK"/></div>

    <div class="col_12"><label for="acceptedTerms" class="col_6"><spring:message
            code="newUserRegistration.label.acceptTerms"/></label>
        <form:checkbox id="acceptedTerms" cssClass="checkbox" cssErrorClass="checkbox error" path="acceptTerms"/></div>
    <div class="col_12">
        <button class="green"><i class="icon-play-circle" type="submit"></i>Register</button>
    </div>
</form:form>
<script type="text/javascript">
    $().ready(function () {
        $("#")
    });
</script>